package com.tms;

import com.tms.SpringcloudGatewayApplication;
import org.junit.jupiter.api.Test;

/**
 * The type Springcloud gateway application test.
 */
class SpringcloudGatewayApplicationTest {

    /**
     * Main.
     */
    @Test
    void main() {
        SpringcloudGatewayApplication.main(new String[] {});

    }
}